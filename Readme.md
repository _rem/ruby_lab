# Ruby lab

When reading an article on Ruby is easy to pop up to the terminal
and explore the code/ideas on them.
Nonetheless, a lot of the times I'm too lazy to do anything else other
than run `pry`. Which means that I miss on a lot of the articles that cover gems
or require writing more code. This is my answer to stop being lazy and get coding.

The included REPL is powered by pry and has access to `lib/experiment.rb`. You can
use any gem within the Gemfile during the REPL session.

## Setting up the skeleton

Clone this repo.

```bash
git clone https://notabug.org/rem/ruby_lab.git
```

Get into the lab.

```bash
cd ruby_lab
```

Install dependencies locally to keep your box clean.

```bash
bundle install --path .bundle/gems
```

Whenever you start a fresh terminal session you'll have to add the `bin`
folder to the PATH once.

```bash
export PATH="$PWD/bin:$PATH"
hash -r 2>/dev/null || true
```

Alternatively, create a simple shell function or alias for them.

Start the Ruby lab:

```bash
repl
```

You can finish your REPL session with `exit`.

You can add your own binstubs to `bin`. Alternatively, you could generate them for
those gems added later which might need to run on their own terminal session.

```bash
bundle binstubs <gem_name_as_in_gemfile>
```

If using rbenv, rehash.

```bash
rbenv rehash
```

### What's included in this lab?

```plain
ruby_lab/
  .gitignore
  .pryrc
  Readme.md
  Gemfile
  lib/
    experiment.rb
  bin/
    repl
```

`.gitignore` in case an experiment takes an interesting turn theres no need to
commit the actual gems.

`.pryrc` Pry configuration file. Defaults to emacs as editor. If you add more files to
`lib` you'll want to list them in here so they're available in the REPL session.

`Gemfile` list of all the gems you want available in the lab. Don't forget to
`bundle install` whenever you add new gems.

`lib/experiment.rb` for whenever the experiments require defining more code than
you can possibly type without making any typos. Since the lab comes packed with
`pry-byebug` you can add `binding.pry` and step over the code in `lib/experiment.rb`

`bin/repl` the binstub that starts the Pry REPL session.

#### ruby_lab shell function

If you want to an easy way of creating a lab perhaps you should consider adding
most of the steps above into a
[simple shell function](https://notabug.org/rem/.dots/src/master/zsh/functions/ruby_lab.sh).

## License

Copyright (c) 2018, René Maya. ISC License. Full copy in [License.md](https://notabug.org/rem/ruby_lab/src/master/License.md)
