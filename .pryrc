# -*- ruby -*-

Pry.config.requires = ["./lib/experiment.rb"]
Pry.config.prompt = proc {
  "#{Pry::Helpers::Text.cyan 'ruby_lab '}"\
  "#{Pry::Helpers::Text.bright_blue '~> '}"
}
